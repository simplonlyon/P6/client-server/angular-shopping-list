import { Injectable } from '@angular/core';
import { Product } from './product';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ProductService {

  constructor(private http: HttpClient) { }
  // ajouter un nouveau produit à une shoppingList
  add(shoppingListId: number, product: Product):Observable<Product>{
    return this.http.post<Product>(`http://localhost:8080/shopping-list/${shoppingListId}/product`, product);
  }
}
